package tdd.training.mra;

import static org.junit.Assert.*;

import org.junit.Test;

import java.util.*;

public class MarsRoverTest {

	@Test
	public void testPlanetContainsObstacleAt() throws MarsRoverException {		
		List<String> planetObstacles = new ArrayList<>();	
		planetObstacles.add(("(4,7)"));
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10,10,planetObstacles);
		boolean obstacle = rover.planetContainsObstacleAt(2,3);
		assertTrue(obstacle);
	    
	}
	
	@Test
	public void testPlanetLanding() throws MarsRoverException {		
		List<String> planetObstacles = new ArrayList<>();	
		planetObstacles.add(("(4,7)"));
		planetObstacles.add("(2,3)");
		String commandString = "0,1,N";
		MarsRover rover = new MarsRover(10,10,planetObstacles);
		String command = rover.executeCommand(commandString);
		assertEquals("(0,1,N)",command);
	    
	}
	
	@Test
	public void testPlanetTurning() throws MarsRoverException {		
		List<String> planetObstacles = new ArrayList<>();	
		planetObstacles.add(("(4,7)"));
		planetObstacles.add("(2,3)");
		String commandString = "r";
		MarsRover rover = new MarsRover(10,10,planetObstacles);
		String command = rover.executeCommand(commandString);
		assertEquals("(0,1,E)",command);
	    
	}
	
	@Test
	public void testPlanetForward() throws MarsRoverException {		
		List<String> planetObstacles = new ArrayList<>();	
		planetObstacles.add(("(4,7)"));
		planetObstacles.add("(2,3)");
		String commandString = "f";
		MarsRover rover = new MarsRover(10,10,planetObstacles);
		String command = rover.executeCommand(commandString);
		assertEquals("(7,7,N)",command);
	    
	}
	
	@Test
	public void testPlanetBackward() throws MarsRoverException {		
		List<String> planetObstacles = new ArrayList<>();	
		planetObstacles.add(("(4,7)"));
		planetObstacles.add("(2,3)");
		String commandString = "b";
		MarsRover rover = new MarsRover(10,10,planetObstacles);
		String command = rover.executeCommand(commandString);
		assertEquals("(4,8,E)",command);
	    
	}

}
