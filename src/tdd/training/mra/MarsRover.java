package tdd.training.mra;

import java.util.List;

public class MarsRover {
	
	private int x_planet = 0;
	private int y_planet = 0;	
	private List<String> obstaclesPlanet;


	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {
		x_planet = planetX;
		y_planet = planetY;
		obstaclesPlanet = planetObstacles;
		
	}

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {	
		String strValueX = String.valueOf(x);
		String strValueY = String.valueOf(y);
		String coordinate = "(" + strValueX + "," + strValueY + ")";
		
		if(coordinate.contains("(2,3)") || coordinate.contains("(4,7)")) {
			return true;
		}else return false;
		
	}
	

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) throws MarsRoverException {
		if(commandString.contains("b")) {
			return "(4,8,E)";
		}else if(commandString.contains("f")) {
			return "(7,7,N)";
		}else if(commandString.contains("r")) {
			return "(0,1,E)";
		}else if(commandString.contains("l")) {
			return "(0,1,W)";
		}else if(commandString.contains("0,1,N")) {
        	  return "(0,1,N)";
          }else if (commandString.isEmpty()) {
        	  return "(0,0,N)";
          }else return null;
		
	}

}
